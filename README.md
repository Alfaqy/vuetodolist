# vuetodolist

# How to use
```
1. Fill in the data list then press + to add data
2. Double tap on data list to delete data

```


 - `raw.https://bitbucket.org/Alfaqy/vuetodolist/src/master/src/assets/image.png?sanitize=true&raw=true`

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
